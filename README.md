# About
This framework is created to automate the test suites in RestAPI. This doesn't limit to just RestAPIs but we can also use the same framework to create UI automated tests.
As of now as part of this assignment, framework is working for RestAPIs automation.

# Tools/Languages used
Entire framework is using serenity-bdd and serenity-rest libraries and associated dependencies.
- serenity-bdd
- serenity-rest
- junit
- java
- GitLab CI
- Apart from the above, there are other libraries which were used

# How to install & run the tests

Clone the repo and URL to clone is https://gitlab.com/srinu-kodi/LeasePlanAssignment
In order to run the tests, we need to have java, maven and an IDE on your machine.
Once repo is cloned, run below command

```
cd LeasePlanAssignment
mvn clean install
```

Note: You can also run CucumberTest file from IDE.

If you don't want to run the tests on your machine, you can also run the pipeline on CI :) :)

# Pipeline Execution Status

`You may see 1 test failure in pipeline as that is -ve scenario which is failing due to the defect in the API.`
Created a pipeline with two stages. The stages are build & test
build - where entire project gets built
test -  where test suite execution happens

Goto pipelines section and hit Run Pipeline button

# How to see the report
You can see the report in below path
target\site\serenity\index.html
Mounted the artifacts so that we can download.
We can find the download artifact option for each pipeline run, we can download from there to local machine and view the index.html.

# What was cleaned up
I have removed below directories/files as they are unnecessary <br>
.m2
gradle
history
target
.DS_Store
gradlew
build.gradle
LICENSE
serenity
.idea
extra README.md

# Refactoring

#### What is refactored
- Feature file
- SearchStepDefinitions
- Package naming
- Removal of CarsAPI file
- Removal of Serenity properties & conf files
- Removal of extra README.md

#### Why it is refactored
- Feature file
    Cucumber BDD feature files talk about behavioural driver development i.e. speaking in end user or business user words.
    So End user don't know what is end point what is internals of the system and hence I have rephrased the scenarios in better manner.
    Written intuitive scenarios which can state the nature of the scenarios & expected outcomes.
- SearchStepDefinitions
    Refactored the scenario steps as per feature scenario steps.
    Written scenario step definitions logic as per the steps only.
- Package naming
    Earlier package naming was starter. It is not intuitive as per this project and hence changed the naming to "com.leasePlan.tests".
    Even created separate packages inside the main package such as pojoModels, api etc...
- Removal of CarsAPI
    We don't need CarsAPI as there is no API specific to Cars and moreover we should have generic API which can handle all types of products like "SearchAPI" and hence I have created it.
    If there is any specific product response changes, then we will create it's pojo model class.
- Removal of Serenity properties file
    Since this is plain rest assured backend API automation suite I didn't feel the need of keeping serenity.properties.
    Moreover, though serenity.conf is present, and as we are not using WebDriver related configs, and hence I have deleted the content inside "serenity.conf".
    If there is any need we can add in the future, but as of now we don't need any data inside serenity.conf by keeping YAGNI principle in mind.