Feature: As a customer
  person visits a store and searches few products
  so that he/she will make a decision what to purchase

#  +ve scenario
  Scenario: [+ve Scenario] Customer wants to search some listed products in the store
    Given store has only "apple" "mango" "tofu" "water" products
    When customer searches for the products related to "apple"
    Then he or she should get the search results for "apple"
    And customer searches for the products related to "mango"
    Then he or she should get the search results for "mango"
    And customer searches for the products related to "tofu"
    Then he or she should get the search results for "tofu"
    And customer searches for the products related to "water"
    Then he or she should get the search results for "water"

#  +ve scenario
# This could be brittle and fail sometimes as I am comparing the actual API response with expected API response.
# It's like we are checking whether data is matching in the system or not.
  Scenario: [+ve Scenario] Customer searched products should match with the products listed in the system
    Given store has only "apple" "mango" "tofu" "water" products
    When customer searches for the products related to "tofu"
    Then search results should match with the "tofu" products listed in the system

# -ve scenario
  Scenario: [-ve Scenario] Customer wants to search some unlisted products in the store
    Given store has only "apple" "mango" "tofu" "water" products
    When customer searches for the unlisted product like "car"
    Then he or she should not get the search results for "car"

#  -ve scenario
#  "Apple" and "apple" are same but API is returning 404 Not found for "Apple". So this scenario is failing when we run.
  Scenario: [-ve Scenario] Customer wants to search the products related to "Apple" which is same as "apple"
    Given store has only "apple" "mango" "tofu" "water" products
    When customer searches for the products related to "Apple"
    Then search results should match with the "apple" products listed in the system

#  -ve scenario
#  Customer checks image on products so that he/she recognize easily which product it is
# So here we are checking if an image exists in the server that means it is present on the product as well
  Scenario: [-ve Scenario] Customer wants to see if all the images of the listed products are present
    Given store has only "apple" "mango" "tofu" "water" products
    When customer searches for the products related to "apple"
    Then customer should be able to see the image on the product for better recognition