package com.leasePlan.tests.stepDefinitions;

import com.leasePlan.tests.api.SearchAPI;
import com.leasePlan.tests.pojoModels.ProductSearchResults;
import com.leasePlan.tests.pojoModels.UnlistedProductSearchResults;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.JSONException;
import org.junit.Assert;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class SearchStepDefinitions extends SearchAPI {

    Response searchResultsResponse;
    int statusCode;
    ProductSearchResults[] listedProductSearchResultsPoJo;
    UnlistedProductSearchResults unlistedProductSearchResultsPoJo;

    @When("store has only {string} {string} {string} {string} products")
    public void givenStoreHasProducts(String product1, String product2, String product3, String product4 ) {

    }

    @When("customer searches for the products related to {string}")
    public void searchProduct(String product) {
        searchResultsResponse = searchGivenProduct(product);
    }
    @When("he or she should get the search results for {string}")
    public void compareProductSearchResultsWithSystem(String product) {
        statusCode = searchResultsResponse.statusCode();
        Assert.assertEquals(200, statusCode);

        listedProductSearchResultsPoJo = searchResultsResponse.as(ProductSearchResults[].class);
        Arrays.stream(listedProductSearchResultsPoJo).forEach(productSearchResults1 -> {
            productSearchResults1.getTitle().contains(product);
            Assert.assertEquals(!productSearchResults1.getBrand().isEmpty(), true);
            Assert.assertEquals(!productSearchResults1.getProvider().isEmpty(), true);
            Assert.assertEquals(!productSearchResults1.getImage().isEmpty(), true);
            Assert.assertEquals(!productSearchResults1.getUnit().isEmpty(), true);
        });
    }

    @When("search results should match with the {string} products listed in the system")
    public void searchResultsShouldMatchWithProductsListedInSystem(String product) throws JSONException, FileNotFoundException {
        statusCode = searchResultsResponse.statusCode();
        Assert.assertEquals(200, statusCode);

        String actualResponseContent = searchResultsResponse.then().extract().asString();
        String expectedJsonContent = new Scanner(new File("src/test/java/com/leasePlan/tests/productData/"+product+".json")).useDelimiter("\\Z").next();

        String expectedJson = new JsonPath(expectedJsonContent).prettyPrint();
        String actualJson = new JsonPath(actualResponseContent).prettyPrint();

        JSONAssert.assertEquals(expectedJson, actualJson, JSONCompareMode.LENIENT);
    }

    @When("customer searches for the unlisted product like {string}")
    public void searchUnlistedProduct(String product) {
        searchResultsResponse = searchGivenProduct(product);
        statusCode = searchResultsResponse.statusCode();
        if (statusCode != 200) {
            System.out.println("Product you are searching for is not listed here");
            unlistedProductSearchResultsPoJo = searchResultsResponse.as(UnlistedProductSearchResults.class);
        }
    }

    @When("he or she should not get the search results for {string}")
    public void verifyUnlistedProductSearchResults(String product) {
        Assert.assertNotEquals(200, statusCode);
        Assert.assertEquals(404, statusCode);
        Assert.assertEquals(unlistedProductSearchResultsPoJo.getDetail().getError(), true);
        Assert.assertEquals(unlistedProductSearchResultsPoJo.getDetail().getMessage(), "Not found");
        Assert.assertEquals(unlistedProductSearchResultsPoJo.getDetail().getRequestedItem(), product);
        Assert.assertEquals(unlistedProductSearchResultsPoJo.getDetail().getServedBy(), "https://waarkoop.com");
    }

    @When("customer should be able to see the image on the product for better recognition")
    public void verifyProductImagesOfSearchResults() {
        statusCode = searchResultsResponse.statusCode();
        Assert.assertEquals(200, statusCode);

        listedProductSearchResultsPoJo = searchResultsResponse.as(ProductSearchResults[].class);
        Arrays.stream(listedProductSearchResultsPoJo).forEach(productSearchResults1 -> {
            String productImageUrl = productSearchResults1.getImage().toString();
            this.verifyProductImagesOnProduct(productImageUrl);
        });
    }

    private void verifyProductImagesOnProduct(String productImageUrl) {
        Response productImageResponse = searchGivenProductImageOnProduct(productImageUrl);
        int imageResponseStatusCode = productImageResponse.statusCode();
        Assert.assertEquals(200, imageResponseStatusCode);
    }

}
