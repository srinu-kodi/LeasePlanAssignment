package com.leasePlan.tests.api;

import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

public class SearchAPI {
    String searchEndpointURL = "https://waarkoop-server.herokuapp.com/api/v1/search/test/";
    public Response searchGivenProduct(String product) {
        return SerenityRest.given()
                .get(searchEndpointURL+product);
    }

    public String searchGivenProducts(String product) {
        return SerenityRest.given()
                .contentType("application/json")
                .header("Content-Type", "application/json")
                .get(searchEndpointURL+product).then().extract().asString();
    }

    public Response searchGivenProductImageOnProduct(String productImageUrl) {
        return SerenityRest.given().get(productImageUrl);
    }
}
