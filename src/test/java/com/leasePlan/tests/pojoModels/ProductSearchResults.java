package com.leasePlan.tests.pojoModels;

public class ProductSearchResults {
    private String provider, title, url, brand, unit, promoDetails, image;
    private boolean isPromo;
    float price;

    public String getBrand() {
        return brand;
    }

    public String getImage() {
        return image;
    }

    public boolean getIsPromo() {
        return isPromo;
    }

    public float getPrice() {
        return price;
    }

    public String getProvider() {
        return provider;
    }

    public String getPromoDetails() {
        return promoDetails;
    }

    public String getTitle() {
        return title;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIsPromo(boolean isPromo) {
        this.isPromo = isPromo;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setPromoDetails(String promoDetails) {
        this.promoDetails = promoDetails;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
